# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server import util


class InlineResponse200(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, api_status: str=None):  # noqa: E501
        """InlineResponse200 - a model defined in Swagger

        :param api_status: The api_status of this InlineResponse200.  # noqa: E501
        :type api_status: str
        """
        self.swagger_types = {
            'api_status': str
        }

        self.attribute_map = {
            'api_status': 'api_status'
        }
        self._api_status = api_status

    @classmethod
    def from_dict(cls, dikt) -> 'InlineResponse200':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The inline_response_200 of this InlineResponse200.  # noqa: E501
        :rtype: InlineResponse200
        """
        return util.deserialize_model(dikt, cls)

    @property
    def api_status(self) -> str:
        """Gets the api_status of this InlineResponse200.


        :return: The api_status of this InlineResponse200.
        :rtype: str
        """
        return self._api_status

    @api_status.setter
    def api_status(self, api_status: str):
        """Sets the api_status of this InlineResponse200.


        :param api_status: The api_status of this InlineResponse200.
        :type api_status: str
        """
        allowed_values = ["up", "down"]  # noqa: E501
        if api_status not in allowed_values:
            raise ValueError(
                "Invalid value for `api_status` ({0}), must be one of {1}"
                .format(api_status, allowed_values)
            )

        self._api_status = api_status
