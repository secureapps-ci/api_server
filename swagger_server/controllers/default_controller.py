import connexion
import six
import json
import yaml

from api_backend.api_actions import APIActions
from swagger_server.models.analysis_abort import AnalysisAbort  # noqa: E501
from swagger_server.models.analysis_create import AnalysisCreate  # noqa: E501
from swagger_server.models.analysis_progress import AnalysisProgress  # noqa: E501
from swagger_server.models.analysis_results import AnalysisResults  # noqa: E501
from swagger_server.models.inline_response200 import InlineResponse200  # noqa: E501
from swagger_server.models.inline_response2001 import InlineResponse2001  # noqa: E501
from swagger_server.models.inline_response2002 import InlineResponse2002  # noqa: E501
from swagger_server.models.tool import Tool  # noqa: E501
from swagger_server.models.tool_create import ToolCreate  # noqa: E501
from swagger_server.models.tool_details import ToolDetails  # noqa: E501
from swagger_server.models.tool_remove import ToolRemove  # noqa: E501
from swagger_server import util

draft_message = 'do some magic!'


def analysis_analysis_id_abort_delete(analysis_id):  # noqa: E501
    """Abort a Analysis by ID

     # noqa: E501

    :param analysis_id: The ID of the Analysis to abort
    :type analysis_id: int

    :rtype: AnalysisAbort
    """
    return draft_message


def analysis_analysis_id_progress_get(analysis_id):  # noqa: E501
    """The Progress of Analysis Stages 1,2,3 and Tool jobs

     # noqa: E501

    :param analysis_id: The ID of the Analysis to retrieve progress
    :type analysis_id: int

    :rtype: AnalysisProgress
    """
    return draft_message


def analysis_analysis_id_results_get(analysis_id):  # noqa: E501
    """Returns results status of Analysis (can include report if complete)

     # noqa: E501

    :param analysis_id: The ID of the Analysis to retrieve results
    :type analysis_id: int

    :rtype: AnalysisResults
    """
    return draft_message


def analysis_analysis_id_stage_stage_name_jobs_get(analysis_id, stage_name):  # noqa: E501
    """Returns Jobs for a specific stage

     # noqa: E501

    :param analysis_id: The ID of the Analysis to retrieve Stages
    :type analysis_id: int
    :param stage_name: The ID of the Stage to return Jobs
    :type stage_name: str

    :rtype: InlineResponse2002
    """
    return draft_message


def analysis_create_post(body=None):  # noqa: E501
    """Create a new Analysis according to the uploaded definition

     # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: AnalysisCreate
    """
    if connexion.request.is_json:
        body = AnalysisCreate.from_dict(connexion.request.get_json())  # noqa: E501

    api_action = APIActions(connexion.request, body)
    return api_action.create_analysis()


def health_get():  # noqa: E501
    """Returns the API health status

     # noqa: E501


    :rtype: InlineResponse200
    """
    return {'api_status': 'up'}


def tool_create_post(body=None):  # noqa: E501
    """Create a new Tool according to the uploaded definition

     # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: ToolCreate
    """
    if connexion.request.is_json:
        body = ToolCreate.from_dict(connexion.request.get_json())  # noqa: E501
    return draft_message


def tool_tool_id_details_get(tool_id):  # noqa: E501
    """Returns details of specific Security Tool

     # noqa: E501

    :param tool_id: The ID of the tool (name:tag)
    :type tool_id: str

    :rtype: ToolDetails
    """
    return draft_message


def tool_tool_id_remove_delete(tool_id):  # noqa: E501
    """Remove the specified Security Tool

     # noqa: E501

    :param tool_id: The ID of the tool (name:tag)
    :type tool_id: str

    :rtype: ToolRemove
    """
    return draft_message


def tools_list_get():  # noqa: E501
    """Collect available Security Tools

     # noqa: E501


    :rtype: List[Tool]
    """
    return draft_message


def version_get():  # noqa: E501
    """Returns the API version

     # noqa: E501


    :rtype: InlineResponse2001
    """
    return {'api_version':	'v1'}
