from connexion.exceptions import OAuthProblem
from private_config.secrets import API_KEY_DB

"""
controller generated to handled auth operation described at:
https://connexion.readthedocs.io/en/latest/security.html
"""


def check_api_key_auth(api_key, required_scopes=None):

    info = API_KEY_DB.get(api_key, None)
    if not info:
        raise OAuthProblem('Invalid token')

    return info
